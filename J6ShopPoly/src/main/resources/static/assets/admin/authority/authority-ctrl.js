app.controller("authority-ctrl",function($scope,  $http, $location){

    $scope.roles = [];
	$scope.admins = [];
	$scope.authorities = [];

    $scope.intilize= function(){
        $http.get("/rest/roles").then(resp =>{
            $scope.roles= resp.data
        })
        //load staff
        $http.get("/rest/accounts?admin=true").then(resp =>{
            $scope.admins = resp.data;
        })
        //load authorities of staff
        //   $http.get("/rest/authorities?admin=true").then(resp =>{
        //    $scope.authorities = resp.data;
        // }).cath(error =>{
        //    // $location.path("/unauthorized");
        // })

        $http.get("/rest/authorities?admin=true").then(resp => {
            			$scope.authorities = resp.data;
            		}).catch(error => {
            			$location.path("/unauthorized");
            		})
    }
    $scope.authority_of = function(acc,role){
        if($scope.authorities){
            return $scope.authorities.find(ur => ur.account.username == acc.username && ur.role.id == role.id);
        }
    }

    $scope.authority_change = function(acc,role){
        var authority = $scope.authority_of(acc,role);
        if(authority){
            $scope.revole_authority(authority);

        }else{
            authority = {account : acc ,role :role};
            $scope.grant_authority(authority);
        }
    }
    //thêm mới 
    $scope.grant_authority = function(authority){
        $http.post(`/rest/authorities`,authority).then(resp =>{
            $scope.authorities.push(resp.data);
            alert("cấp quyền sử dụng thành công")
        }).cath(error =>{
            alert("cấp quyền sử dụng thất bại")
            console.log("Error",error)
        })
    }
    //xóa
    $scope.revole_authority= function(authority){
        $http.delete(`/rest/authorities/${authority.id}`).then(resp =>{
            var index = $scope.authorities.findIndex(a => a.id == authority.id);
            $scope.authorities.splice(index,1);
            alert("thu hồi quyền thành công")
        }).cath(error => {
            alert("thu hồi quyền thất baị")
            console.log("Error",error)
        })
    }
$scope.intilize();
})


// $scope.roles = [];
// 	$scope.admins = [];
// 	$scope.authorities = [];

// 	$scope.initialize = function() {
// 		//load all roles
// 		$http.get("/rest/roles").then(resp => {
// 			$scope.roles = resp.data
// 		})
		
// 		//load staff and directors (administrators)
// 		$http.get("/rest/accounts?admin=true").then(resp => {
// 			$scope.admins = resp.data;
// 		})
		
// 		//load authorities of staff and directors
// 		$http.get("/rest/authorities?admin=true").then(resp => {
// 			$scope.authorities = resp.data;
// 		}).catch(error => {
// 			$location.path("/unauthorized");
// 		})
// 	}
// 	$scope.authority_of = function(acc,role){
// 		if($scope.authorities){
// 			return $scope.authorities.find(ur => ur.account.username == acc.username && ur.role.id == role.id);
// 		}
// 	}
// 	$scope.authority_changed = function(acc,role){
// 		var authority = $scope.authority_of(acc,role);
// 		if(authority){//đã cấp quyền => thu hồi quyền xóa
// 			$scope.revole_authority(authority);
// 		}else{//chưa cấp quyền => cấp quyền mới
// 			authority = {account : acc, role: role};
// 			$scope.grant_authority(authority);
// 		}
// 	}
// 	//Thêm mới authority
// 	$scope.grant_authority = function(authority){
// 		$http.post(`/rest/authorities`,authority).then(resp => {
// 			$scope.authorities.push(resp.data);
// 			alert("Cấp quyền sử dụng thành công")
// 		}).catch(error => {
// 			alert("Cấp quyền sự dụng thất bại")
// 			console.log("Error", error)
// 		})
// 	}
	
// 	//Xóa authority
// 	$scope.revole_authority = function(authority){
// 		$http.delete(`/rest/authorities/${authority.id}`).then(resp => {
// 			var index = $scope.authorities.findIndex(a => a.id == authority.id);
// 			$scope.authorities.splice(index,1);
// 			alert("Thu hồi quyền sử dụng thành công")
// 		}).catch(error => {
// 			alert("Thu hồi quyền sự dụng thất bại")
// 			console.log("Error", error)
// 		})
// 	}
// 	$scope.initialize();
// })