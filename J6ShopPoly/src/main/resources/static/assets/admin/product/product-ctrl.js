app.controller("product-ctrl", function($scope, $http){
$scope.items=[];
$scope.cates=[];
$scope.form=[];


$scope.initialize = function(){
    //load product
    $http.get("/rest/products").then(resp =>{
        $scope.items= resp.data;
        $scope.items.forEach(item => {
            item.createDate = new Date(item.createDate)
        })
    });
    //load caterory
    $http.get("/rest/categories").then(resp =>{
        $scope.cates = resp.data;
    });

}
$scope.initialize();

$scope.reset = function(){
    $scope.form={
        createDate : new Date(),
        image:'14.png',
        availabel:true,
    };
}
$scope.edit = function(item){
    $scope.form= angular.copy(item);
    $(".nav-tabs a:eq(0)").tab('show')


}
$scope.create =function(){
      var item =angular.copy($scope.form);
      $http.post('/rest/products', item).then(resp =>{
          resp.data.createDate = new Date(resp.data.createDate)
          $scope.items.push(resp.data);
          $scope.reset();
          $scope.initialize();
          alert("Add product successfully");
      }).catch(error =>{
        alert("Add product fail");
        console.error("Error",error);
      });
        
    }
$scope.update = function(){
  var item = angular.copy($scope.form);
  $http.put(`/rest/products/${item.id}`,item).then(resp =>{
      var index  = $scope.items.findIndex(p => p.id == item.id);
      $scope.items[index] = item;
      alert("cập nhật thành công!");
  }).catch(error =>{
      alert("cập nhật sản phẩm lỗi!")
      console.log("Error",error);
  })  
}
$scope.delete = function(item){
   $http.delete(`/rest/products/${item.id}`).then(resp =>{
        var index  = $scope.items.findIndex(p => p.id == item.id);
        $scope.items.splice(index,1);
        $scope.reset();
        alert("Xóa sản phẩm thành công!");
    }).catch(error =>{
        alert("Xóa sản phẩm thất bại!")
        console.log("Error",error);
    })   
}
// $scope.imageChanged = function(files){
//     var data = new FormData();
//     data.append('file',files[0]);
//     $http.post('/rest/upload/images',data,{
//         transformRequest: angular.identity,
//         Headers:{'Content-Type': undefined}
//     }).then(resp =>{
//         $scope.form.image = resp.data.name;
//     }).catch(error =>{
//         alert("lỗi upload hình ảnh");
//         console.log("Error",error);
//     })
//     }
    
$scope.imageChanged = function(files){
    var data = new FormData();
    data.append('file', files[0]);
    $http.post('/rest/upload/images', data,{
        transformRequest: angular.identity, 
        headers:{'Content-Type': undefined}
    }).then(resp => {
        $scope.form.image = resp.data.name;
    }).catch(error => {
        alert("Lỗi upload hình ảnh")
        console.log("Error", error)
    })
}
$scope.pager ={
    page : 0,
    size : 5,
   get items(){
			var start = this.page * this.size;
			return $scope.items.slice(start, start + this.size);
		},
    get count(){
        return Math.ceil(1.0 * $scope.items.length / this.size);
    },
    first(){
        this.page =0;
    },
    prev(){
        this.page--;
        if(this.page < 0){
            this.last();
        }
    },
    next(){
        this.page++;
        if(this.page >= this.count){
            this.first();
        }
    },
   last(){
        this.page = this.count -1;
    }
}

})