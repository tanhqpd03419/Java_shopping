package J6.com.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import J6.com.entity.Category;

@Repository
public interface CategoryDAO extends JpaRepository<Category, Integer>{
	
	List<Category> findByName(String name);
	
}
