package J6.com.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import J6.com.entity.Role;

public interface RoleDAO extends JpaRepository<Role, String>{

}
