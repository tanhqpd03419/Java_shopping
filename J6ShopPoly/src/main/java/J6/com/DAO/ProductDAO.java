package J6.com.DAO;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import J6.com.entity.Product;
@Repository
public interface ProductDAO extends JpaRepository<Product, Integer>{
	@Query("SELECT p FROM Product p WHERE p.category.id =?1")
	List<Product>findByCategoryId(Integer cid);
	
	List<Product> findByName(String name);
	
	@Query("SELECT o FROM Product o WHERE o.name LIKE ?1") 
	  Page<Product> findByNamePage(String keywords, Pageable pageable); 
}
