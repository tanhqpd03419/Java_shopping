package J6.com.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import J6.com.entity.Account;

public interface AccountDAO extends JpaRepository<Account, String>{
	@Query("select DISTINCT ar.account from Authorities ar WHERE ar.role.id IN ('DIRE','STAFF')")
	List<Account> getAdministrators();
	
	@Query("SELECT a FROM Account a WHERE a.email = ?1")
	Optional<Account> findByEmail(String email);
	

}
