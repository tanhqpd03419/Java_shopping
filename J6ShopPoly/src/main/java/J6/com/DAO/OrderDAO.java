package J6.com.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import J6.com.entity.Order;

public interface OrderDAO extends JpaRepository<Order, Long>{
	@Query("select o from Order o where o.account.username = ?1")
	List<Order> findByUsername(String username);
	
	
//	@Query("SELECT o FROM Order o WHERE o.status =1 or o.status=2 or o.status=3")
//	List<Order> findOrderByStatus();
//	@Query("SELECT o FROM Order o WHERE o.status =0")
//	List<Order> findOrderByStatus0();
//	@Query("SELECT count(*) FROM Order o WHERE o.status =0")
//	Integer CountOrder0();
	
}
