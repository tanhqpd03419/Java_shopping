package J6.com.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import J6.com.entity.Account;
import J6.com.entity.Authorities;

public interface AuthoritiesDAO extends JpaRepository<Authorities, Integer>{
	@Query("SELECT DISTINCT a FROM Authorities a WHERE a.account IN ?1")
	List<Authorities> authoritiesOf(List<Account> accounts);
}
