package J6.com.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import J6.com.entity.OrderDetail;

public interface OrderDetailDAO extends JpaRepository<OrderDetail, Long> {

}
