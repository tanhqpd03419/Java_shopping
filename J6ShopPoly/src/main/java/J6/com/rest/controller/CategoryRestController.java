package J6.com.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import J6.com.DAO.ProductDAO;
import J6.com.Service.CategoryService;
import J6.com.Service.ProductService;
import J6.com.entity.Category;
import J6.com.entity.Product;

@CrossOrigin("*")
@RestController
@RequestMapping("rest/categories")
public class CategoryRestController {
	@Autowired CategoryService categoryService;
	
	
	@GetMapping()
	public List<Category> getAll() {
		return categoryService.findAll();
	}
	@GetMapping("{id}")
	public Category finbyid(@PathVariable("id")Integer id) {
		return categoryService.findById(id).get();
	}
	
	@GetMapping("{name}")
	public List<Category> finbyname(@PathVariable("name")String name) {
		return categoryService.findByName(name);
	}
	
	@PostMapping()
	public Category postproduct(@RequestBody Category category) {
		return categoryService.save(category);
	}
	
	@PutMapping("{id}")
	public Category update(@PathVariable("id")Long id,@RequestBody Category category) {
		return categoryService.save(category);
	}
	@DeleteMapping("{id}")
	public void deleteProduct(@PathVariable("id")Integer id) {
		categoryService.deleteById(id);
	}
}

