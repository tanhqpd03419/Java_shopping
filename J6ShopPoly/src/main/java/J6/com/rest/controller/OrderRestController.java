package J6.com.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;

import J6.com.Service.OrderService;
import J6.com.Service.ProductService;
import J6.com.entity.Order;
import J6.com.entity.Product;

@CrossOrigin("*")
@RestController
@RequestMapping("rest/orders")
public class OrderRestController {
@Autowired 
OrderService  orderService;
	
	@PostMapping()
	public Order create(@RequestBody JsonNode orderData) {
		return orderService.create(orderData);
		
	}	

//	@GetMapping("")
//	public List<Order> get() {
//		return orderService.findAll();
//	}
//	@GetMapping("order0")
//	public List<Order> get0() {
//		return orderService.findOrderByStatus0();
//	}
//	@GetMapping("count")
//	public Integer count() {
//		return orderService.CountOrder0();
//	}
//	@GetMapping("{id}")
//	public Order finbyid(@PathVariable("id")Long id) {
//		return orderService.findById(id);
//		
//	}
//	
//	@PutMapping("{id}")
//	public Order update(@PathVariable("id")Long id,@RequestBody Order order) {
//		return orderService.save(order);
//	}
//	@DeleteMapping("{id}")
//	public void deleteOrder(@PathVariable("id")Long id) {
//		orderService.deleteById(id);
//	}
}
