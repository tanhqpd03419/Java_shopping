package J6.com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import J6.com.Interceptor.GloballInterceptor;
@Configuration
public class InterceptorConfig implements WebMvcConfigurer{
@Autowired
GloballInterceptor globallInterceptor;

@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(globallInterceptor)
		.addPathPatterns("/**")
	    .excludePathPatterns("/rest/**","/admin/**","/assets/**");
	}


}
