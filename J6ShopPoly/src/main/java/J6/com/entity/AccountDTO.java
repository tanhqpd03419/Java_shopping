package J6.com.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDTO implements Serializable {
  	String username;
    String password;
    String fullname;
	String phone;
    String email;
    String photo;
    Boolean enable;
    String address;
    String verification_code;

Date register_date = new Date();

}
