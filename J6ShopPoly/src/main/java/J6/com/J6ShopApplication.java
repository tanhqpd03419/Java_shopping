package J6.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J6ShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(J6ShopApplication.class, args);
	}

}
