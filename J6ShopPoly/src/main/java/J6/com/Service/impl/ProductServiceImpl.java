package J6.com.Service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import J6.com.DAO.ProductDAO;
import J6.com.Service.ProductService;
import J6.com.entity.Product;
@Service
public class ProductServiceImpl implements ProductService {
//@Autowired
//ProductDAO pdao;
//
//@Override
//public List<Product> findByCategoryId(String cid) {
//	return pdao.findByCategoryId(cid);
//}
//
//public ProductServiceImpl(ProductDAO pdao) {
//	
//	this.pdao = pdao;
//}
//
//@Override
//public <S extends Product> S save(S entity) {
//	return pdao.save(entity);
//}
//
//@Override
//public <S extends Product> Optional<S> findOne(Example<S> example) {
//	return pdao.findOne(example);
//}
//
//@Override
//public Page<Product> findAll(Pageable pageable) {
//	return pdao.findAll(pageable);
//}
//
//@Override
//public List<Product> findAll() {
//	return pdao.findAll();
//}
//
//@Override
//public List<Product> findAll(Sort sort) {
//	return pdao.findAll(sort);
//}
//
//@Override
//public List<Product> findAllById(Iterable<Integer> ids) {
//	return pdao.findAllById(ids);
//}
//
//
//@Override
//public Product findById(Integer id) {
//	return pdao.findById(id).get();
//}
//
//@Override
//public <S extends Product> S saveAndFlush(S entity) {
//	return pdao.saveAndFlush(entity);
//}
//
//@Override
//public <S extends Product> Page<S> findAll(Example<S> example, Pageable pageable) {
//	return pdao.findAll(example, pageable);
//}
//
//@Override
//public void delete(Product entity) {
//	pdao.delete(entity);
//}
//
//@Override
//public Product getById(Integer id) {
//	return pdao.getById(id);
//}
//
//@Override
//public <S extends Product> List<S> findAll(Example<S> example, Sort sort) {
//	return pdao.findAll(example, sort);
//}
	@Autowired ProductDAO productDAO;

	
	@Override
	public Page<Product> findAll(Pageable pageable) {
		return productDAO.findAll(pageable);
	}

	@Override
	public List<Product> findByName(String name) {
	return productDAO.findByName(name);
}

	@Override
	public List<Product> findAll() {
		return productDAO.findAll();
	}

	@Override
	public Product findById(Integer id) {
		return productDAO.findById(id).get();
	}

	@Override
	public Page<Product> findByNamePage(String keywords, Pageable pageable) {
		return productDAO.findByNamePage(keywords, pageable);
	}

	@Override
	public List<Product> findByCategoryId(Integer cid) {
		return productDAO.findByCategoryId(cid);
	}

	@Override
	public Product create(Product product) {
		return productDAO.save(product);
	}

	@Override
	public Product update(Product product) {
		return productDAO.save(product);
	}

	@Override
	public void delete(Integer id) {
		 productDAO.deleteById(id);
	}

}
