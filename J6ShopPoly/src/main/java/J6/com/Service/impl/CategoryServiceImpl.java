package J6.com.Service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import J6.com.DAO.CategoryDAO;
import J6.com.Service.CategoryService;
import J6.com.entity.Category;
@Service
public class CategoryServiceImpl implements CategoryService{
@Autowired
CategoryDAO cdao;

public CategoryServiceImpl(CategoryDAO cdao) {
	
	this.cdao = cdao;
}

@Override
public <S extends Category> S save(S entity) {
	return cdao.save(entity);
}

@Override
public <S extends Category> Optional<S> findOne(Example<S> example) {
	return cdao.findOne(example);
}

@Override
public List<Category> findAll() {
	return cdao.findAll();
}

@Override
public List<Category> findAllById(Iterable<Integer> ids) {
	return cdao.findAllById(ids);
}

@Override
public List<Category> findByName(String name) {
	return cdao.findByName(name);
}

@Override
public Optional<Category> findById(Integer id) {
	return cdao.findById(id);
}

@Override
public <S extends Category> List<S> saveAll(Iterable<S> entities) {
	return cdao.saveAll(entities);
}

@Override
public <S extends Category> Page<S> findAll(Example<S> example, Pageable pageable) {
	return cdao.findAll(example, pageable);
}

@Override
public void deleteById(Integer id) {
	cdao.deleteById(id);
}

@Override
public void delete(Category entity) {
	cdao.delete(entity);
}

@Override
public Category getById(Integer id) {
	return cdao.getById(id);
}

}
