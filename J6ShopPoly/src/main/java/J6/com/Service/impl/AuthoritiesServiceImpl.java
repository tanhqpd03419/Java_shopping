package J6.com.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import J6.com.DAO.AccountDAO;
import J6.com.DAO.AuthoritiesDAO;
import J6.com.Service.AuthoritiesService;
import J6.com.entity.Account;
import J6.com.entity.Authorities;
@Service
public class AuthoritiesServiceImpl implements AuthoritiesService{
 @ Autowired
 AuthoritiesDAO authoritiesDAO;
 @Autowired
 AccountDAO accountDAO;

@Override
public <S extends Authorities> S save(S entity) {
	return authoritiesDAO.save(entity);
}

@Override
public List<Authorities> findAll() {
	return authoritiesDAO.findAll();
}

@Override
public void deleteById(Integer id) {
	authoritiesDAO.deleteById(id);
}
@Override
public List<Authorities> findAuthoritiesOfAdministrators() {
	List<Account> accounts = accountDAO.getAdministrators();
	return authoritiesDAO.authoritiesOf(accounts);
}
 
}
