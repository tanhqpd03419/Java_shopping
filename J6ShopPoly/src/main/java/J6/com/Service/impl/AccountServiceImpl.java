package J6.com.Service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import J6.com.DAO.AccountDAO;
import J6.com.Service.AccountService;
import J6.com.entity.Account;
@Service
public class AccountServiceImpl implements AccountService {
@Autowired
AccountDAO accountDAO;



public AccountServiceImpl(AccountDAO accountDAO) {
	
	this.accountDAO = accountDAO;
}


@Override
public List<Account> getAdministrators() {
	return accountDAO.getAdministrators();
}


@Override
public <S extends Account> S save(S entity) {
	return accountDAO.save(entity);
}

@Override
public List<Account> findAll() {
	return accountDAO.findAll();
}

@Override
public Account findById(String username) {
	return accountDAO.findById(username).get();
}

@Override
public <S extends Account> long count(Example<S> example) {
	return accountDAO.count(example);
}

@Override
public long count() {
	return accountDAO.count();
}

@Override
public void deleteById(String id) {
	accountDAO.deleteById(id);
}

@Override
public Account getOne(String id) {
	return accountDAO.getOne(id);
}

@Override
public Account getById(String id) {
	return accountDAO.getById(id);
}
@Override
public Optional<Account> findByEmail(String email) {
	// TODO Auto-generated method stub
	return accountDAO.findByEmail(email);
}
	
}
