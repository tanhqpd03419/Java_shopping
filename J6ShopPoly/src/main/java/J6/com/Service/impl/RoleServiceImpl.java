package J6.com.Service.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import J6.com.DAO.RoleDAO;
import J6.com.Service.RoleService;
import J6.com.entity.Role;
@Service
public class RoleServiceImpl implements RoleService {
@Autowired
RoleDAO roleDAO;

@Override
public List<Role> findAll() {
	return roleDAO.findAll();
}

}
