package J6.com.Service;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

import J6.com.entity.Order;

public interface OrderService {

	List<Order> findByUsername(String username);

	Order findById(Long id);

	Order create(JsonNode orderData);

	List<Order> findAll();

	

	void deleteById(Long id);

	<S extends Order> S save(S entity);

	
}
