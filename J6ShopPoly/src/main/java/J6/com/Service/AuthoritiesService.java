package J6.com.Service;

import java.util.List;

import J6.com.entity.Authorities;

public interface AuthoritiesService {

	void deleteById(Integer id);

	List<Authorities> findAll();

	<S extends Authorities> S save(S entity);

	List<Authorities> findAuthoritiesOfAdministrators();

}
