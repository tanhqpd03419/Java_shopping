package J6.com.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Example;

import J6.com.entity.Account;

public interface AccountService {

	Account getById(String id);

	Account getOne(String id);

	void deleteById(String id);

	long count();

	<S extends Account> long count(Example<S> example);

	

	List<Account> findAll();

	<S extends Account> S save(S entity);

	Account findById(String username);

	List<Account> getAdministrators();

	Optional<Account> findByEmail(String email);
	

}
