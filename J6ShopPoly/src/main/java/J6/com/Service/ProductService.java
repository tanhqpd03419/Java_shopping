package J6.com.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;


import J6.com.entity.Product;


public interface ProductService {

	void delete(Integer id);

	Product update(Product product);

	Product create(Product product);

	List<Product> findByCategoryId(Integer cid);

	Product findById(Integer id);

	List<Product> findAll();
//
//	<S extends Product> List<S> findAll(Example<S> example, Sort sort);
//
//	Product getById(Integer id);
//
//	void delete(Product entity);
//
//	<S extends Product> Page<S> findAll(Example<S> example, Pageable pageable);
//
//	<S extends Product> S saveAndFlush(S entity);
//
//	
//
//	List<Product> findAllById(Iterable<Integer> ids);
//
//	List<Product> findAll(Sort sort);
//
//	List<Product> findAll();
//
//	Page<Product> findAll(Pageable pageable);
//
//	<S extends Product> Optional<S> findOne(Example<S> example);
//
//	<S extends Product> S save(S entity);
//
//	List<Product> findByCategoryId(String cid);
//
//	Product findById(Integer id);

	List<Product> findByName(String name);

	Page<Product> findByNamePage(String keywords, Pageable pageable);

	Page<Product> findAll(Pageable pageable);

	

}
