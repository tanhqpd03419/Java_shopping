package J6.com.Service;

import java.util.List;

import J6.com.entity.Role;

public interface RoleService {

	List<Role> findAll();

}
