package J6.com.Controller;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import J6.com.Service.ProductService;
import J6.com.entity.Product;



@Controller
public class ProductController {
	@Autowired
	ProductService productService;
	
 @RequestMapping("/product/list")
 public String list(Model model,@RequestParam("cid") Optional<Integer> cid) {
	 if (cid.isPresent()) {
		 List<Product> list =productService.findByCategoryId(cid.get());
		 model.addAttribute("items",list);
	}
	 else {
		 List<Product> list =productService.findAll();
		 model.addAttribute("items",list);
	 }
	return"product/list" ;
 }
 @RequestMapping("/product/detail/{id}")
 public String detail(Model model ,@PathVariable("id") Integer id) {
	Product item =productService.findById(id);
	model.addAttribute("item", item);
	 return"product/detail" ;
 }
 @RequestMapping("/search")
	public String search(Model model,@RequestParam(name="name",required = false) String name) {
		List<Product> list = null;
		if(StringUtils.hasText(name)) {
			list =productService.findByName(name);
			model.addAttribute("list", list);
			
		}else {
			list =productService.findAll();
		}
		model.addAttribute("items", list);
		
		return "product/list";
	}
 @RequestMapping("/searchpaginated")
	public String search(ModelMap model,@RequestParam(name="name",required = false) String name,
			@RequestParam("page") Optional<Integer> page,
			@RequestParam("size") Optional<Integer> size) {
		int currentPage =page.orElse(1);
		int pageSize = size.orElse(5);
		
		Pageable pageable =PageRequest.of(currentPage, pageSize,Sort.by("name"));
		Page<Product> resultPage = null;
		
		
		if(StringUtils.hasText(name)) {
			resultPage =productService.findByNamePage(name,pageable);
			model.addAttribute("items",name);
		}else {
			resultPage =productService.findAll(pageable);
		}
		
		int totalPages= resultPage.getTotalPages();
		if(totalPages>0) {
			int start = Math.max(1, currentPage-2);
			int end =Math.min(currentPage +2, totalPages);
			
			if(totalPages >5) {
				if(end == totalPages) start =end -5;
				else if(start ==1) end =start+5;
			}
			List<Integer>pageNumbers =IntStream.rangeClosed(start, end)
					.boxed()
					.collect(Collectors.toList());
			model.addAttribute("pageNumbers",pageNumbers);
		}
		model.addAttribute("productPage", resultPage);
		
		return "product/list";
	}
	
	
	

}

