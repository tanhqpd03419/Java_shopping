package J6.com.Controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import J6.com.Service.AccountService;
import J6.com.Service.AuthoritiesService;
import J6.com.entity.Account;
import J6.com.entity.AccountDTO;
import J6.com.entity.Authorities;
import J6.com.entity.Role;

@Controller
public class SecurityController {
	@Autowired 
	AccountService accountService;
	@Autowired 
	AuthoritiesService authoritiesService;
	
@RequestMapping("security/login/form")
public String loginForm(Model model) {
	model.addAttribute("message","Vui lòng đăng nhập!");
	return "security/login";
}
@RequestMapping("/security/login/success")
public String loginSuccess(Model model) {
	model.addAttribute("message","Vui lòng đăng nhập!");
	return "security/login";
}
@RequestMapping("/security/login/error")
public String loginError(Model model) {
	model.addAttribute("message","sai thông tin đăng nhập!");
	return "security/login";
}
@RequestMapping("/security/unauthoried")
public String unauthoried(Model model) {
	model.addAttribute("message","không có quyền truy xuất");
	return "security/login";
}
@RequestMapping("/security/logoff/success")
public String loggoffSuccess(Model model) {
	model.addAttribute("message", "Đăng xuất thành công!");
	return "security/login";
}

@RequestMapping("security/register/form")
public String registerForm(Model model) {
	model.addAttribute("message","Vui lòng đăng nhập!");
	return "security/register";
}
@RequestMapping("security/register")
public String register(Model model) {
	model.addAttribute("message1","đăng ki thanh công!");
	return "security/login";
}
//@RequestMapping("security/register/form")
//public ModelAndView postsignupForm(ModelMap model,@ModelAttribute("account") AccountDTO dto,HttpServletRequest request)  {
////	model.addAttribute("message", "Vui lòng đăng nhập");
//	Optional<Account> opt = accountService.findByEmail(dto.getEmail());
//	if (opt.isPresent()) {
//		System.out.println("present");
//		model.addAttribute("message", "Email đã tồn tại");
//		return new ModelAndView("security/login",model);
//		
//	}else {
//		Authorities au = new Authorities();
//		Role rl =  new Role();
//		
//		Account account = new Account();
//		
//		BeanUtils.copyProperties(dto, account);
////		account.setPassword(bCryptPasswordEncoder.encode(dto.getPassword()));
//		account.setPassword(dto.getPassword());
//		
//		account.setPhoto("");
//	
//		au.setAccount(account);
//		rl.setId("USER");
//		au.setRole(rl);
//		
//		accountService.save(account);
//		authoritiesService.save(au);
//		model.addAttribute("message", "Vui lòng verify trước khi đăng nhập");
//		return new ModelAndView("security/register",model);
//	}
//	
//}

}
